<!--
## README FIRST

Title this issue with `L&D Request: [Topic].
-->

## Learning & Development Request 

Please fill out this request template if you would like to collaborate with the Learning & Development team on an idea for a topic to be covered or if you would like to cover/present on a topic as a subject matter expert. 

Our team will review and set the priority for your request based on the scale and impact across the organization. Learning items that will be applied and used across the company will take priority. You can learn more about how we prioritize requests on our [handbook page](https://about.gitlab.com/handbook/people-group/learning-and-development/#how-the-ld-team-prioritizes-requests). 

If you just need assistance with scheduling an event, you can use the process that we follow and [have outlined](https://gitlab.com/gitlab-com/people-group/learning-and-development/-/blob/master/scheduling-learning-events/index.html.md). 

### What type of learning solution is your request for? (i.e. Live Learning, Certification, Video, Knowledge Assessment, etc)
*  insert answer here

### What topic do you want to cover? 
*  insert answer here

### What painpoints will covering this topic solve? Why do we need this?  
*  insert answer here

### Who will benefit from this request being fulfilled? (i.e. all GitLab team members, managers, finance team members, etc.) 
*  insert answer here

## Live Learning (delete this section if it does not apply)

### If you are requesting a Live Learning, can the topic be done in one of our [Live Learning formats](https://about.gitlab.com/handbook/people-group/learning-and-development/#live-learning)? 
*  [ ] Yes 
*  [ ] No 

### If you are requesting a Live Learning, who will present the content for this session (who is/will be the subject matter expert)?
*  insert answer here

#### If the Subject Matter Expert (SME) is not yourself, have you confirmed that the SME is willing to lead the session?
*  [ ] Yes 
*  [ ] No 
*  [ ] N/A 

### Scheduling the Live Learning session

You can use the process that we follow and [have outlined](https://gitlab.com/gitlab-com/people-group/learning-and-development/-/blob/master/scheduling-learning-events/index.html.md) to get your event scheduled.  

## Video (delete this section if it does not apply)

### What are your ideas for the Video
*  Add ideas here 

## Knowledge Assessment (delete this section if it does not apply)

### If you are requesting a Knowledge Assessemnt, has the Google Form already been created? 
*  [ ] Yes 
*  [ ] No 

If so, please add the link to the google form here:

If not, please review some of our current [knowledge assessments](https://about.gitlab.com/handbook/people-group/learning-and-development/certifications/#current-knowledge-assessments) to confirm this is what you are looking for. Then review our [steps to create a knowledge assessment](https://about.gitlab.com/handbook/people-group/learning-and-development/certifications/#how-to-create-a-knowledge-assessment) to get started. 

## Certification (delete this section if it does not apply)

### If you are requesting a Certification, have you reviewed our differentiation between [certifications](https://about.gitlab.com/handbook/people-group/learning-and-development/certifications/#certifications) and [knowledge assessments](https://about.gitlab.com/handbook/people-group/learning-and-development/certifications/#knowledge-assessments) to confirm that you need a certification? 
*  [ ] Yes 
*  [ ] No 

### What is the outline you are wanting to have for the certification? 
*  Add ideas here 

### Knowledge Assessments for Certifications

Knowledge assessments are a piece of certifications. 

*  Add any links to knowledge assessments you have already set up here. If you do not have any set up, review our [steps to create a knowledge assessment](https://about.gitlab.com/handbook/people-group/learning-and-development/certifications/#how-to-create-a-knowledge-assessment) to get started.  


/label ~"Learning & Development"
/label ~"L&D - Requests"
