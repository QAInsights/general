<!--
## README FIRST

Title this issue with `New L&D Team Member: [First & Last Name].
Assign this issue to new L&D team members.
-->

## Learning & Development 

Welcome to the Learning & Development team at GitLab. We are so excited to have you! This onboarding issue outlines key items about Learning and Development at GitLab. Please complete this issue during your second week at GitLab. 

### Responsibilities 

- Maintains the back-end management of learning administration to include certifications, knowledge assessments, external learning inquires, and internal training programs
- Enhance and build out our Handbook resources for learning
- Develops Learning Management System (LMS) adoption guides for team members and enablement activities with guidance from the L&D Partner
- Supports the L&D Generalist with integrating the LMS into GitLab. Utilizes the LMS course authoring tool to create Handbook first course content
- Identify, develop, and deliver programs that support the growth of our team members and GitLab
- Develop customized learning paths for team members that incorporate customized and curated learning content
- Supports the marketing strategy and develops communication material to promote internal training
- Works with the L&D Generalist and Partner to develop a comprehensive GitLab learning curriculum that incorporates multi-modality learning formats (bite-sized training, virtual instructor-led, hands-on workshops, self-study, etc) while ensuring all material is Handbook first
- Track and monitor training consumptions through reporting and analytics
- Solicit and incorporate team member feedback into our course content and learning experience

**Content creation**

- Start thinking of teams we can partner with to identify organizational learning needs (e-group, functional groups, etc)
- Develop bite size training modules based off existing information in the handbook
- Support L&D team in the design and development of role based learning paths using Handbook material and off-the-shelf content

### Skills Needed for Successes
- GitLab tech savviness and ability to use and teach others on how to use the GitLab tool
- Clear and concise communication with strong interpersonal skills
- Strong organizational skills
- Results orientated
- Learner-first mindset
- Manager of One
- Bias for action
- Collaboration
- Distilling complex information into digestible parts
- People centric

### 30-60-90 Day Plan 

This is high-level and not exhaustive at all! 

**30 Days:** 
- Immerse into L&D Associate role
- Complete training and onboarding issue
- Familiarize with L&D priorities and issue board
- Familiarize with EdCast Learning Management System and join implementation meetings 
- Identify focus areas for learning content development and prioritities
- Review manual tasks and identify areas we can automate 
- Support L&D Generalist in identifying manual tasks and including into day-to-day activities in the role

**60 Days:**
- Ownership of manual tasks 
- Begin developing learning content (micro-learning, live learning, etc) 
- Attend LMS training 
- Support LMS integration efforts 
- Solicit and incoporate team member feedback into course content
- Identify and develop areas to improve learning & development experience, handbook, content, etc
- Develop L&D marketing and communications plan to help us carve out the Remote Learning space

**90 Days:**
- Own LMS integration efforts with L&D Generalist
- Develop course content in multi-modality formats
- Deliver course content in multi-modality formats
- Automate manual tasks with integration of the LMS
- Implement L&D marketing and communications plan
- The Sky is the limit! 


----------------------------------------------------------------

## Tasks to Complete

### New Team Member Tasks
*  [ ] Review [L&D page](https://about.gitlab.com/handbook/people-group/learning-and-development/) 
*  [ ] Review [L&D Roadmap](https://about.gitlab.com/handbook/people-group/learning-and-development/#gitlab-learning-strategy--roadmap)
*  [ ] Review [L&D issue board](https://gitlab.com/gitlab-com/people-group/learning-and-development/-/boards/1281903)
* [ ] Read the [comprehensive guide to Learning and Development](https://www.digitalhrtech.com/learning-and-development/) by Digital HR Tech 
* [ ] Read the Harvard Business Review article on [Where Companies Go Wrong with Learning and Development](https://hbr.org/2019/10/where-companies-go-wrong-with-learning-and-development)
* [ ] Read the McKinsey & Company's article on the [essential components of successful L&D strategy](https://www.mckinsey.com/business-functions/organization/our-insights/the-essential-components-of-a-successful-l-and-d-strategy)
* [ ] Checkout and bookmark the [Chief Learning Officer](https://www.chieflearningofficer.com/) website. It is an industry leading site for learning and development!
- [ ] Bookmark the following Learning Blogs/Sites...these are some of our favorites but there's a lot out there! 
    - [Learning in the Moderrn Workplace](http://www.c4lpt.co.uk/blog/)
    - [Emerald Works Research](https://emeraldworks.com/)
    - [LinkedIn Learning Blog](https://www.linkedin.com/business/learning/blog)
* [ ] Expense [LinkedIn Learning](https://www.linkedin.com/learning/paths/become-an-l-d-professional-4) and complete the [Becoming an L&D Professional Learning Path](https://www.linkedin.com/learning/paths/become-an-l-d-professional-4) over the course of the next few weeks
* [ ] [Checkout Vyond](https://www.vyond.com/solutions/training-and-elearning-videos/) and sign up for an account, this is what we use to create fun animation videos. Josh will invite you to our business account. Vyond offers [free webinars](https://www.vyond.com/webinars/) to learn more about the service. Consider signing up for ones that interest you. 
- [ ] Sign up for an account with [Articulate 360](https://360.articulate.com/). We will use this service to create eLearning for our team members. Check out the [example of a finished course](https://360.articulate.com/review/content/ede7dcbf-7f25-48b1-a889-85645e2cb96d/review). We don't have a business account yet but will have one in the next 2 weeks. Free training [videos are here](https://training.articulate.com/videos). 
- [ ] Be on the lookout for invites to meetings on your calendar! There is a lot going on with the [EdCast Implementation](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/457) that we want the L&D Associate role to help drive
- [ ] Ensure L&D Partner/Generalist invite you to all the necessary learning related slack channels!


### Career Development
* [ ] Read the [career development page](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/) and become familiar with the content
* [ ] Use the career mapping and development resources to start defining what you want to acheive during your first year with the L&D team. Discuss with your manager during a weekly 1:1 when you are ready to share
* [ ] The L&D Team has been using time on Friday's for professional development. The learning industry changes so fast and there are a lot of great tools, learning formats, etc out there to learn about. Carve out time on Friday to develop your career. 

### Existing L&D Team Member Tasks
*  [ ] Add to `learning@gitlab.com` alias (access request)
*  [ ] Access to Zapier login info 
*  [ ] Add to L&D Folder in Google Drive 
*  [ ] Access to WILL Learning
*  [ ] Invite to all L&D related Slack channels
*  [ ] Access to Vyond 

/label ~"Learning & Development"
